// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// DEVELOPER: Cesar Abascal
// PROFESSORS: Cesar Augusto Prior and Cesar Rodrigues (Yeah. Its almost an overflow!)
// PROJECT: Olimex EKG/EMG - Eletrocardiogram/Eletromiogram waves acquisition
// ARCHIVE: Scketch to acquire and send olimex data via serial
// REFERENCE: This scketch is based on ShieldEkgEmgDemo.ino writed by Penko Bozhkov
// DATE: 13/03/2019
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


// OLIMEX PACKET FORMAT ------------------------------------------------------------------
// Version 2
// 17-byte packets are transmitted from Arduino at 256Hz,
// using 1 start bit, 8 data bits, 1 stop bit and no parity.
// Structure of olimex_packet:
//struct olimex_packet{
//  uint8_t	sync0;		  // Sync 0 byte = 0xa5
//  uint8_t	sync1;		  // Sync 1 byte = 0x5a
//  uint8_t	version;	  // Packet Version = 2
//  uint8_t	count;		  // Packet counter that increases by 1 each packet
//  uint16_t	data[6];  // 10-bit sample (0 to 1023) in big endian format
//  uint8_t	switches;	  // State of PD5 to PD2, in bits 3 to 0.
//}; // Here we've 17-byte unsigned

// TRANSMISSION SPEED --------------------------------------------------------------------
// Example of calculation of minimum transmission speed:
// A sample is taken every 8ms (ie. 125 samples per second)
// 125 sps * sizeof(olimex_packet) = 2125 bytes per second (Bps)
// 2125 Bps = 2.125 kBps = 17000 bits per second (bps)
// 765 kBph =~ 0.765 MB per hour


// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// LIBRARIES -----------------------------------------------------------------------------
#include <compat/deprecated.h> // This contains several items that used to be available in
                               //previous versions of this scketch, but have eventually
                               //been deprecated over time.
#include <FlexiTimer2.h> // This library enable Arduino to use timer 2 with configurable
                         //resolution. http://www.arduino.cc/playground/Main/FlexiTimer2

// CONSTANTS -----------------------------------------------------------------------------
#define SAMPFREQ 200              // ADC sampling rate. CHANGE SAMPLE FREQ HERE
#define TIMER2VAL (1024/SAMPFREQ) // Set ADC sample rate frequency
#define TXSPEED 57600             // Serial communication speed. USE 57600
// ---
#define NUMCHANNELS 6
#define HEADERLEN 4
#define PACKETLEN ((NUMCHANNELS*2)+HEADERLEN+1) // Packetlen = 17
// ---
#define LED 13 // Arduino Led L
#define CAL_SIG 9

// GLOBAL VARIABLES ----------------------------------------------------------------------
volatile unsigned char TXBuf[PACKETLEN]; // Transmission packet
volatile unsigned char TXIndex;          // Byte index used in transmission packet
volatile unsigned char CurrentCh;        // Current channel being sampled
volatile unsigned char counter = 0;      // Additional divider used to generate CAL_SIG
volatile unsigned int ADC_Value = 0;	   // ADC current value

// FUNCTIONS -----------------------------------------------------------------------------
// This is used just to toggle led L
void toggleLED(void){
 if(digitalRead(LED) == HIGH){
   digitalWrite(LED, LOW);
 }else{
   digitalWrite(LED, HIGH);
 }
}

// This is used to switches-over GAL_SIG
void toggle_GAL_SIG(void){
 if(digitalRead(CAL_SIG) == HIGH){
  digitalWrite(CAL_SIG, LOW);
 }else{
  digitalWrite(CAL_SIG, HIGH);
 }
}

// This is used to read values and send data over serial communication
void timerTwoOverflowISR(){
  // Toogle LED L with ADC sampling frequency /2
  toggleLED();
  
  // Read the 6 ADC inputs and store current values in packet
  for(CurrentCh = 0; CurrentCh < 6; CurrentCh++){
    ADC_Value = analogRead(CurrentCh);
    TXBuf[((2 * CurrentCh) + HEADERLEN)] = ((unsigned char)((ADC_Value & 0xFF00) >> 8)); // Write high byte
    TXBuf[((2 * CurrentCh) + HEADERLEN + 1)] = ((unsigned char)(ADC_Value & 0x00FF));	// Write low byte
  }

  // Send packet over serial
  for(TXIndex=0;TXIndex<PACKETLEN;TXIndex++) {
    Serial.write(TXBuf[TXIndex]);
  }

  // Increment the packet counter. The counter will return to 0 after 255 increments. Overflow!
  TXBuf[3]++;

  // Generate the CAL_SIGnal
  counter++; // Increment the devider counter
  if(counter == 12){  // 256/12/2 = 10.6Hz -> Toggle frequency
    counter = 0;
    toggle_GAL_SIG(); // Generate CAL signal with frequ ~10Hz
  }
}

// This is used to reset TXBuf to default values
void resetTXBuf() {
  TXBuf[0] = 0xa5;  // Sync 0
  TXBuf[1] = 0x5a;  // Sync 1
  TXBuf[2] = 2;     // Protocol version
  TXBuf[3] = 0;     // Packet counter
  TXBuf[4] = 0x02;  // CH1 High Byte
  TXBuf[5] = 0x00;  // CH1 Low Byte
  TXBuf[6] = 0x02;  // CH2 High Byte
  TXBuf[7] = 0x00;  // CH2 Low Byte
  TXBuf[8] = 0x02;  // CH3 High Byte
  TXBuf[9] = 0x00;  // CH3 Low Byte
  TXBuf[10] = 0x02; // CH4 High Byte
  TXBuf[11] = 0x00; // CH4 Low Byte
  TXBuf[12] = 0x02; // CH5 High Byte
  TXBuf[13] = 0x00; // CH5 Low Byte
  TXBuf[14] = 0x02; // CH6 High Byte
  TXBuf[15] = 0x00; // CH6 Low Byte
  TXBuf[16] = 0x01; // Switches state
}

// ARDUINO SETUP and LOOP ----------------------------------------------------------------
// In setup arduino will initializes all peripherals
void setup(){
  // Disable all interrupts before initialization
  noInterrupts();
  
  // Setup Arduino LED L
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);

  // Setup CAL_SIG
  pinMode(CAL_SIG, OUTPUT);
  digitalWrite(CAL_SIG, LOW);

  // Reset TXBuf
  resetTXBuf();

  // Set Timer2, that is used to setup the analag channels sampling frequency
  //and packet update. Whenever interrupt occures, the current read packet is
  //sent over serial communication.
  FlexiTimer2::set(TIMER2VAL, timerTwoOverflowISR);
  FlexiTimer2::start();

  // Starts the communication with the previously defined speed
  Serial.begin(TXSPEED);

  // Enable all interrupts after initialization has been completed
  interrupts();
}

// In principal loop MCU will be put into sleep mode
void loop() {
  __asm__ __volatile__ ("sleep");
}
