#!/usr/bin/env python3

## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## DEVELOPER: Cesar Abascal
## PROFESSORS: Cesar Augusto Prior and Cesar Rodrigues (Yeah. Its almost an overflow!)
## PROJECT: Olimex EKG/EMG - Eletrocardiogram/Eletromiogram waves acquisition
## ARCHIVE: Simple script to plot acquired data.
## DATE: 12/03/2019
## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


## LIBRARIES ----------------------------------------------------------------------------
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import butter, lfilter, lfilter_zi


# SCRIPT FUNCTIONS ----------------------------------------------------------------------
# Read csv file
def getExGSignal():

    sRate = 200 # Samples per second
    ch1, ch2, ch3 = [], [], []
    ch4, ch5, ch6 = [], [], []

    patientName = str(input("Patient name: "))
    fileDir = "exg-data/raw-exg/" + patientName + "/ExG_signals.csv"

    samples = 0
    with open(fileDir) as dataFile:
        next(dataFile)
        for line in dataFile:
            aux = line.split(';')
            ch1.append(int(aux[0]))
            ch2.append(int(aux[1]))
            ch3.append(int(aux[2]))
            ch4.append(int(aux[3]))
            ch5.append(int(aux[4]))
            ch6.append(int(aux[5]))
            samples +=1
        #end-for
    #end-with

    dataFile.close()

    # Generate X Axis.
    xAxis = np.linspace(0, samples/sRate, samples, endpoint=True)

    return xAxis, ch1, ch2, ch3, ch4, ch5, ch6, samples, sRate, patientName
#end def

# Butterworth filter
def butter_bandpass(lowcut, highcut, sRate, order=5):
    nyq = 0.5 * sRate
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a
#end def

#def butter_bandpass_filter(data, lowcut, highcut, sRate, order=5):
#    b, a = butter_bandpass(lowcut, highcut, sRate, order=order)
#    y = lfilter(b, a, data)
#    return y
##end def

# This function will apply the filter considering the initial transient.
def butter_bandpass_filter_zi(data, lowcut, highcut, sRate, order=5):
    b, a = butter_bandpass(lowcut, highcut, sRate, order=order)
    zi = lfilter_zi(b, a)
    y,zo = lfilter(b, a, data, zi=zi*data[0])
    return y
#end def

# Plot channels
def plotChannels(ax, ch1, ch2, ch3, ch4, ch5, ch6, name, stype):
    plt.figure('Six '+stype+' Olimex ECG Channels from '+name, figsize=(14,6))

    plt.subplot(3,2,1)
    plt.title("Channel 1")
    plt.ylabel("amplitude")
    plt.plot(ax, ch1, "red")
    plt.grid()

    plt.subplot(3,2,2)
    plt.title("Channel 2")
    plt.plot(ax, ch2, "green")
    plt.grid()

    plt.subplot(3,2,3)
    plt.title("Channel 3")
    plt.ylabel("amplitude")
    plt.plot(ax, ch3, "black")
    plt.grid()

    plt.subplot(3,2,4)
    plt.title("Channel 4")
    plt.plot(ax, ch4, "purple")
    plt.grid()

    plt.subplot(3,2,5)
    plt.title("Channel 5")
    plt.xlabel("time (s)")
    plt.ylabel("amplitude")
    plt.plot(ax, ch5, "orange")
    plt.grid()

    plt.subplot(3,2,6)
    plt.title("Channel 6")
    plt.xlabel("time (s)")
    plt.plot(ax, ch6, "blue")
    plt.grid()

    plt.draw()
#end-def


# MAIN ----------------------------------------------------------------------------------

# Get data
x, c1, c2, c3, c4, c5, c6, samp, sps, patient = getExGSignal()

# Apply bandpass filter into raw signals
lowcut = 0.3 # 0.5
highcut = 7 # 8
order = 2
c1f = butter_bandpass_filter_zi(c1, lowcut, highcut, sps, order)
c2f = butter_bandpass_filter_zi(c2, lowcut, highcut, sps, order)
c3f = butter_bandpass_filter_zi(c3, lowcut, highcut, sps, order)
c4f = butter_bandpass_filter_zi(c4, lowcut, highcut, sps, order)
c5f = butter_bandpass_filter_zi(c5, lowcut, highcut, sps, order)
c6f = butter_bandpass_filter_zi(c6, lowcut, highcut, sps, order)

# Plot raw signals
plotChannels(x, c1, c2, c3, c4, c5, c6, patient, "Raw")
# Plot filtered signals
plotChannels(x, c1f, c2f, c3f, c4f, c5f, c6f, patient, "Filtered")
# Show graphics
plt.show()